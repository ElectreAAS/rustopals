use rand::{Rng, RngCore};
use regex::Regex;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, Read, Write};

const LETTER_FREQUENCIES_EN: &str =
    " et,'.!?aoinshrdlcuI\nmwfgypbvkjxqzETAONSRHLDCUMFPGWYBVKXJQZ\"-0123456789();:";

const WEIGHT_UNKNOWN_LETTER: usize = 1000;

/// Pad a binary number with zeroes until its size is a multiple of size.
/// If little_endian is true, pad on the right.
pub fn pad_bin(s: &mut String, size: usize, litte_endian: bool) {
    let n = s.len() % size;
    if n != 0 {
        let to_add = "\u{0}".repeat(size - n);
        if litte_endian {
            s.push_str(&to_add);
        } else {
            s.insert_str(0, &to_add);
        }
    }
}

/// Pad a byte sequence with zeroes until its size is a multiple of size.
/// If little_endian is true, pad on the right.
pub fn pad_bytes(s: &mut Vec<u8>, size: usize, litte_endian: bool) {
    let n = s.len() % size;
    if n != 0 {
        s.append(&mut vec![0; size - n]);
        if !litte_endian {
            s.rotate_right(size - n)
        }
    }
}

/// Pad given byte sequence 's' up to a multiple of 'size' bytes, using PKCS#7 scheme.
pub fn pad_pkcs7(s: &mut Vec<u8>, size: u8) {
    let n = (s.len() % size as usize) as u8;
    if n != 0 {
        let byte = size - n;
        for _ in 0..byte {
            s.push(byte);
        }
    }
}

/// Try to unpad 's' as if it was padded with PKCS#7 scheme.
pub fn unpad_pkcs7(s: &mut Vec<u8>) {
    let last = s[s.len() - 1] as usize;
    if 0 < last && last < s.len() {
        let suffix = vec![last as u8; last];
        if s.ends_with(&suffix[..]) {
            let new_size = s.len() - last;
            s.truncate(new_size);
        }
    }
}

/// Validate the padding on given block.
/// Return Err in case of bad padding.
pub fn validate_pkcs7(s: &[u8]) -> Result<Vec<u8>, String> {
    let err_msg = String::from("Invalid padding!");
    let mut s2 = s.to_owned();
    unpad_pkcs7(&mut s2);
    if *s == s2 {
        Err(err_msg)
    } else {
        Ok(s2)
    }
}

/// Weight the string 'msg' within the scope of LETTER_FREQUENCIES_EN.
pub fn weight_freqs(msg: String) -> usize {
    msg.chars()
        .fold(0, |acc, c| match LETTER_FREQUENCIES_EN.find(c) {
            Some(x) => acc + x,
            None => acc + WEIGHT_UNKNOWN_LETTER,
        })
}

/// Computes the binary hamming distance between the two byte sequences.
pub fn hamming_dist(l: &[u8], r: &[u8]) -> u32 {
    assert_eq!(l.len(), r.len());
    l.iter()
        .zip(r)
        .fold(0, |sum, (x, y)| (x ^ y).count_ones() + sum)
}

/// Break down the byte sequence 's' as rows of 'size' bytes, and transpose it.
pub fn transpose(size: usize, s: &[u8]) -> Vec<Vec<u8>> {
    let mut res = Vec::with_capacity(size);
    let full_lines = s.len() / size;
    let rest = s.len() % size;
    for column in 0..size {
        let index = if column < rest { 1 } else { 0 };
        let mut v = Vec::with_capacity(full_lines + index);
        for line in 0..v.capacity() {
            v.push(s[column + line * size]);
        }
        res.push(v);
    }
    res
}

/// Reads the entire file into a string, if last parameter is true, removes new_lines.
pub fn read_file(file_name: &str, remove_nl: bool) -> String {
    let mut f = File::open(file_name).unwrap();
    let mut s = String::new();
    f.read_to_string(&mut s).unwrap();
    if remove_nl {
        s.replace("\n", "")
    } else {
        s
    }
}

/// Generate a random byte sequence of size n.
/// Will use supplied random number generator if present, otherwise the default one.
pub fn random_bytes(n: usize, rng: Option<&mut dyn RngCore>) -> Vec<u8> {
    match rng {
        Some(rng) => (0..n).map(|_| rng.gen()).collect(),
        None => (0..n).map(|_| rand::random()).collect(),
    }
}

/// If given char is printable, print it and flush.
/// Otherwise add it to buff for next prints.
#[allow(dead_code)]
pub fn may_print_char(c: u8, buff: &mut Vec<u8>) {
    buff.push(c);
    if let Ok(s) = String::from_utf8(buff.to_vec()) {
        print!("{}", s);
        io::stdout().flush().unwrap();
        buff.clear();
    }
}

/// Parse a structured-cookie-like string (foo=bar&pi=3) into a map.
pub fn cookie_parse(s: &str) -> HashMap<String, String> {
    let regexp = Regex::new(r"([a-zA-Z]\w*)=([\w@\.\-%]+)").unwrap();
    let mut m: HashMap<String, String> = HashMap::new();
    for group in regexp.captures_iter(s) {
        m.insert(group[1].to_string(), group[2].to_string());
    }
    m
}

/// Format a map into a structured-cookie-like string (foo=bar&pi=3).
pub fn cookie_format(m: HashMap<String, String>) -> Option<String> {
    let id: usize = m.get("uid")?.parse().unwrap();
    let s = format!(
        "email={}&uid={:03}&role={}",
        m.get("email")?,
        id,
        m.get("role")?
    );
    Some(s)
}

/// Create a placeholder profile for a standard user, with given email adress.
pub fn create_profile(mut email: String) -> HashMap<String, String> {
    email.retain(|c| c != '&' && c != '=');
    let mut m = HashMap::with_capacity(3);
    m.insert(String::from("email"), email);
    m.insert(String::from("uid"), rand::random::<u8>().to_string());
    m.insert(String::from("role"), String::from("user"));
    m
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::conversions as cv;

    #[test]
    fn transpose_test() {
        let range: Vec<u8> = (0..16).collect();
        let v = transpose(4, &range);
        let should_be = [[0, 4, 8, 12], [1, 5, 9, 13], [2, 6, 10, 14], [3, 7, 11, 15]];
        assert_eq!(v, should_be);

        let range: Vec<_> = (0..8).collect();
        let v = transpose(3, &range);
        let should_be = vec![vec![0, 3, 6], vec![1, 4, 7], vec![2, 5]];
        assert_eq!(v, should_be);
    }

    #[test]
    fn hamming_dist_test() {
        let l = cv::text_to_bytes(String::from("this is a test"));
        let r = cv::text_to_bytes(String::from("wokka wokka!!!"));
        assert_eq!(hamming_dist(&l, &r), 37);
    }

    #[test]
    fn cookie_parse_test() {
        let m = cookie_parse("foo=bar&baz=qux&a=b");
        assert!(m.get("foo").is_some());
        assert_eq!(m.get("foo").unwrap(), "bar");
        assert!(m.get("baz").is_some());
        assert_eq!(m.get("baz").unwrap(), "qux");
        assert!(m.get("a").is_some());
        assert_eq!(m.get("a").unwrap(), "b");

        let m2 = cookie_parse("a=b&email=AuroraBorealis@_thistimeofyear");
        assert!(m2.get("a").is_some());
        assert_eq!(m2.get("a").unwrap(), "b");
        assert!(m2.get("email").is_some());
        assert_eq!(m2.get("email").unwrap(), "AuroraBorealis@_thistimeofyear");
    }

    #[test]
    fn cookie_format_test() {
        let mut m = HashMap::with_capacity(2);
        m.insert(String::from("email"), String::from("foo"));
        m.insert(String::from("role"), String::from("b"));
        m.insert(String::from("uid"), String::from("65"));
        let res = cookie_format(m);
        assert!(res.is_some());
        assert_eq!(res.unwrap(), String::from("email=foo&uid=065&role=b"));

        let mut m2 = HashMap::with_capacity(2);
        m2.insert(String::from("email"), String::from("foo"));
        m2.insert(String::from("role"), String::from("b&cheat=enabled"));
        m2.insert(String::from("uid"), String::from("65"));
        let res = cookie_format(m2);
        assert!(res.is_some());
        assert_eq!(
            res.unwrap(),
            String::from("email=foo&uid=065&role=b&cheat=enabled")
        );
    }

    #[test]
    fn create_profile_test() {
        let profile = create_profile(String::from("foo@bar.com"));
        let email = profile.get("email");
        assert!(email.is_some());
        assert_eq!(email.unwrap(), "foo@bar.com");
        let role = profile.get("role");
        assert!(role.is_some());
        assert_eq!(role.unwrap(), "user");

        let profile2 = create_profile(String::from("foo@bar.com&role=admin"));
        let email2 = profile2.get("email");
        assert!(email2.is_some());
        assert_eq!(email2.unwrap(), "foo@bar.comroleadmin");
        let role2 = profile2.get("role");
        assert!(role2.is_some());
        assert_eq!(role2.unwrap(), "user");
    }
}
