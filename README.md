# Rustopals, Cryptopals in rust!

This codebase is a transfer of my other project [here](https://gitlab.com/ElectreAAS/Cryptopals) from Clojure to Rust.
All explanations are done there for now.
As rust is much better suited for this endeavour, I'll probably work more here than there, but for now both will stay.
